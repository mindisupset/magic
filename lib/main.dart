import 'dart:math';
import 'package:flutter/material.dart';

void main() => runApp(
      MyApp(),
    );
class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Text('Magic'),
          backgroundColor: Colors.teal,
        ),
        body: Magicpage(),
      ),
    );
  }
}
class Magicpage extends StatefulWidget {
  @override
  _MagicpageState createState() {
    return _MagicpageState();
  }
}
class _MagicpageState extends State<Magicpage> {
  int ball=1;

  @override
  Widget build(BuildContext context) {

    return Container(
      child: Center(
        child: Row(
          children: [
            Expanded(
              child: GestureDetector(
                onTap: () {
                  setState(() {
                    ball = Random().nextInt(6)+ 1;
                  });
                  print('Answer is ${ball.toString()}');
                },
                child: Padding(
                  padding: EdgeInsets.all(16.0),
                  child: Image(image: AssetImage("assets/images/ball${ball.toString()}.png")),
                ),
              ),
            ),

          ],
        ),
      ),
    );
  }
}